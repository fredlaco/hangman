<?php

namespace AppBundle\Game;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameContext implements GameContextInterface
{
    const SESSION_KEY = 'hangman_game_context';

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session  = $session;
    }

    public function reset()
    {
        $this->session->set(self::SESSION_KEY, array());
    }

    public function newGame($word)
    {
        return new Game($word);
    }

    public function loadGame()
    {
        $data = $this->session->get(self::SESSION_KEY);

        if (!$data) {
            return false;
        }

        return new Game(
            $data['word'],
            $data['attempts'],
            $data['tried_letters'],
            $data['found_letters']
        );
    }

    public function save(Game $game)
    {
        $this->session->set(self::SESSION_KEY, $game->getContext());
    }
}
