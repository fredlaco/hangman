<?php

namespace AppBundle\Controller;

use AppBundle\Game\GameContext;
use AppBundle\Game\GameRunner;
use AppBundle\Game\Loader\TextFileLoader;
use AppBundle\Game\Loader\XmlFileLoader;
use AppBundle\Game\WordList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * @Route("/", name="game")
     */
    public function indexAction(Request $request)
    {
        $list = new WordList();
        $list->addLoader('txt', new TextFileLoader());
        $list->addLoader('xml', new XmlFileLoader());
        $list->loadDictionaries([
            __DIR__.'/../../../app/Resources/data/test.txt',
            __DIR__.'/../../../app/Resources/data/words.txt',
            __DIR__.'/../../../app/Resources/data/words.xml',
        ]);

        $runner = new GameRunner(new GameContext($request->getSession()), $list);

        return $this->render('game/index.html.twig', [
            'game' => $runner->loadGame(),
        ]);
    }

    /**
     * @Route("/won", name="game_won")
     */
    public function wonAction()
    {
        return $this->render('game/won.html.twig');
    }

    /**
     * @Route("/failed", name="game_failed")
     */
    public function failedAction()
    {
        return $this->render('game/failed.html.twig');
    }

    /**
     * @Route("/letter/{letter}", name="game_play_letter")
     */
    public function playLetterAction(Request $request, $letter)
    {
        $runner = new GameRunner(new GameContext($request->getSession()));

        $runner->playLetter($letter);

        return $this->redirectToRoute('game');
    }
}
