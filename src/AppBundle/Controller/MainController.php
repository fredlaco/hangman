<?php

namespace AppBundle\Controller;

use AppBundle\Contact\Message;
use AppBundle\Form\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('main/index.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactType::class)
            ->handleRequest($request)
        ;

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Message $message */
            $message = $form->getData();

            $mail = \Swift_Message::newInstance(
                'Message envoyé depuis Hangman.com',
                $message->getContent()
            );

            $mail->setReplyTo($message->getAuthorEmail(), $message->getAuthorName());
            $mail->setTo('toto@titi.fr');

            $this->get('mailer')->send($mail);
        }

        return $this->render('main/contact.html.twig', [
            'contact_form' => $form->createView(),
        ]);
    }
}
