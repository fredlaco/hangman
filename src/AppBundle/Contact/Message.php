<?php

namespace AppBundle\Contact;

use Symfony\Component\Validator\Constraints as Assert;

class Message
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private $authorName = '';

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @Assert\Email
     */
    private $authorEmail = '';

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min=20, max=1000)
     */
    private $content = '';

    public function getAuthorName()
    {
        return $this->authorName;
    }

    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }
}
